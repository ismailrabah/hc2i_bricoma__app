# Hc2i_bricoma_app BMAS

<h4>Manager artisans services application</h4>
Web application (<b>BRICOMA</b> Local deployment)
<p>(needes: POS local application database structure)</p>
<br/>
<h5>Admin Space</h5>
<ul>
    <li>Study POS local application database structure<b>(4h)</b></li>
    <li>Synchronize data (invoice code, date ) from MSQL local server<b>(25h)</b></li>
    <li>Manage clients (CRUD<b>(8h)</b>) 
        <ul>
            <li>Form to register new client / client address<b>(6h)</b></li>
            <li>Orders history and statistics <b>(12h)</b></li>
        </ul>
    </li>
    <li>Manage artisans (CRUD<b>(8h)</b>)
        <ul>
            <li>Assign to order<b>(5h)</b></li>
            <li>Assigned orders details<b>(7h)</b></li>
            <li>Specialities and services<b>(5h)</b></li>
        </ul>
    </li>
    <li>Manage Orders 
        <ul>
            <li>Manage order status and progress history by date/hour and status (New, onHold, assigned, doing, done, cancel )<b>(25h)</b> </li>
            <li>Generate artisan services invoice (PDF)<b>(8h)</b></li>
        </ul>
    </li>
    <li>Users login/register and roles(Admin, manager, artisan, "client"), permissions<b>(20h)</b></li>
    <li>Create and configure a cron job to synchronize data each (hour/day/week/month)<b>(30h)</b></li>
</ul>
<h5>Artisans Space</h5>
<ul>
    <li>Login and register as artisan<b>(15h)</b></li>
    <li>Assigned Order details<b>(5h)</b>
        <ul>
            <li>Show client address/location, name and phone number<b>(5h)</b></li>
            <li>Changer order progress and status to(doing/done)<b>(20h)</b></li>
            <li>Request order cancelation<b>(12h)</b></li>
        </ul>
    </li>
</ul>



<h3>The time estimation is between 45 and 55 days<h3/>


<small style="float: right"> <a href="https://hc2i.tech/"> &#169;Hc2i Maroc 	&#174;</a></small>